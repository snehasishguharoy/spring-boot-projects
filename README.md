This Repository consists of :
1. Spring Boot with Spring REST
2. Spring Boot with Spring MVC
3. Spring Boot with Spring Core
4.  Spring Boot with Mockito
5.  Spring Boot with Junits
6.  Spring Boot with Spring AOP
7.  Spring Boot with Gradle
8.  Spring Boot with Spring Data JPA and Spring JDBC
9.   Spring Boot with Spring Boot Basic Features
