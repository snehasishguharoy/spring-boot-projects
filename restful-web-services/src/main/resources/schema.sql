insert into user_details(id,birth_date,name) values (1001,current_date(),'Snehasish');
insert into user_details(id,birth_date,name) values (1002,current_date(),'Ravi');
insert into user_details(id,birth_date,name) values (1003,current_date(),'Satish');

insert into user_post(id,description,user_id) values(2001,'Java Post',1001);
insert into user_post(id,description,user_id) values(2002,'AWS Post',1002);
insert into user_post(id,description,user_id) values(2003,'GCP Post',1003);
insert into user_post(id,description,user_id) values(2004,'Mysql Post',1001);
insert into user_post(id,description,user_id) values(2005,'Devops Post',1002);
insert into user_post(id,description,user_id) values(2006,'PL/SQL Post',1003);