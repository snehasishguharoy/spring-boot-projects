package com.example.rest.webservices.restfulwebservices.beans;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

@Data
@AllArgsConstructor
@ToString
public class PersonV1 {

    private String name;

}
