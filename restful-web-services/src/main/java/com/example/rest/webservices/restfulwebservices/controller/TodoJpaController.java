package com.example.rest.webservices.restfulwebservices.controller;


import com.example.rest.webservices.restfulwebservices.beans.Todo;
import com.example.rest.webservices.restfulwebservices.repo.TodoRepo;
import com.example.rest.webservices.restfulwebservices.service.TodoService;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class TodoJpaController {

    private TodoService todoService;

    private TodoRepo todoRepo;

    public TodoJpaController(TodoService todoService, TodoRepo todoRepo) {
        this.todoService = todoService;
        this.todoRepo = todoRepo;
    }

    @GetMapping("/users/{username}/todos")
    public List<Todo> retrieveTodos(@PathVariable String username) {
        return todoRepo.findByUsername(username);
    }

    @GetMapping("/users/{username}/todos/{id}")
    public Todo retrieveTodo(@PathVariable String username, @PathVariable int id) {
        return todoRepo.findById(id).get();
    }

    @DeleteMapping("/users/{username}/todos/{id}")
    public ResponseEntity<Void> deleteTodo(@PathVariable String username, @PathVariable int id) {
        todoRepo.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    @PutMapping("/users/{username}/todos/{id}")
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public Todo updateTodo(@PathVariable String username, @PathVariable int id, @RequestBody Todo todo) {
        todoRepo.save(todo);
        return todo;
    }

    @PostMapping("/users/{username}/todos")
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public Todo createTodo(@PathVariable String username, @RequestBody Todo todo) {
        todo.setUsername(username);
        return todoRepo.save(todo);
    }
}
