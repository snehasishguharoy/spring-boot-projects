package com.example.rest.webservices.restfulwebservices.dao;

import com.example.rest.webservices.restfulwebservices.user.User;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class UserDaoService {

    private static List<User> list = new ArrayList<>();
    private static Integer usersCount = 0;

    static {

        list.add(new User(++usersCount, "Snehasish", LocalDate.now().minusYears(30)));
        list.add(new User(++usersCount, "Samik", LocalDate.now().minusYears(35)));
        list.add(new User(++usersCount, "Rahul", LocalDate.now().minusYears(21)));
    }


    public List<User> getAllUsers() {
        return list;
    }

    public User findOne(Integer id) {
        return list.stream().filter(t -> t.getId().equals(id)).findFirst().orElse(null);
    }

    public User save(User user) {

        user.setId(++usersCount);
        list.add(user);
        return user;
    }

    public void deleteById(Integer id) {
        list.removeIf(t -> t.getId().equals(id));
    }
}