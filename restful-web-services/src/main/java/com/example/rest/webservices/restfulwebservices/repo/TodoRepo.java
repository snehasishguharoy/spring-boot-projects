package com.example.rest.webservices.restfulwebservices.repo;

import com.example.rest.webservices.restfulwebservices.beans.Todo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TodoRepo extends JpaRepository<Todo,Integer> {
    List<Todo> findByUsername(String username);
}
