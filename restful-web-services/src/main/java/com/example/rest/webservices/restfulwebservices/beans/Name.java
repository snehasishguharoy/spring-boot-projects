package com.example.rest.webservices.restfulwebservices.beans;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

@Data
@AllArgsConstructor
@ToString
public class Name {
    private String fname;
    private String lname;
}
