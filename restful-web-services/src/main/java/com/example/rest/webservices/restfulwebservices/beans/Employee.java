package com.example.rest.webservices.restfulwebservices.beans;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;


@Data
@AllArgsConstructor
@ToString
// static filtering
//@JsonIgnoreProperties({"salary", "age"})
@JsonFilter("EmpFilter")
public class Employee {

    private String name;
    private Integer age;
    //@JsonIgnore
    private Double salary;
}
