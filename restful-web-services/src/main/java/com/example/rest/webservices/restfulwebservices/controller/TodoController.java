//package com.example.rest.webservices.restfulwebservices.controller;
//
//
//import com.example.rest.webservices.restfulwebservices.beans.Todo;
//import com.example.rest.webservices.restfulwebservices.repo.TodoRepo;
//import com.example.rest.webservices.restfulwebservices.service.TodoService;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.*;
//
//import java.util.List;
//
////@RestController
//public class TodoController {
//
//    private TodoService todoService;
//
//    private TodoRepo todoRepo;
//
//    public TodoController(TodoService todoService, TodoRepo todoRepo) {
//        this.todoService = todoService;
//        this.todoRepo = todoRepo;
//    }
//
//    @GetMapping("/users/{username}/todos")
//    public List<Todo> retrieveTodos(@PathVariable String username) {
//        return todoService.findByUserName(username);
//    }
//
//    @GetMapping("/users/{username}/todos/{id}")
//    public Todo retrieveTodo(@PathVariable String username, @PathVariable int id) {
//        return todoService.findById(id, username);
//    }
//
//    @DeleteMapping("/users/{username}/todos/{id}")
//    public ResponseEntity<Void> deleteTodo(@PathVariable String username, @PathVariable int id) {
//        todoService.deleteById(id, username);
//        return ResponseEntity.noContent().build();
//    }
//
//    @PutMapping("/users/{username}/todos/{id}")
//    public Todo updateTodo(@PathVariable String username, @PathVariable int id, @RequestBody Todo todo) {
//        todoService.updateTodo(todo);
//        return todo;
//    }
//
//    @PostMapping("/users/{username}/todos")
//    public Todo createTodo(@PathVariable String username,@RequestBody Todo todo) {
//        Todo createdTodo = todoService.addTodo(todo.getUsername(), todo.getDescription(), todo.getTargetDate(), todo.isDone());
//        return createdTodo;
//    }
//}
