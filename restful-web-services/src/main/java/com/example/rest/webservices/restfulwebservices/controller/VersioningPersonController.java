package com.example.rest.webservices.restfulwebservices.controller;

import com.example.rest.webservices.restfulwebservices.beans.Name;
import com.example.rest.webservices.restfulwebservices.beans.PersonV1;
import com.example.rest.webservices.restfulwebservices.beans.PersonV2;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class VersioningPersonController {

    @GetMapping("/v1/person")
    public PersonV1 getFirstVersionOfPerson() {
        return new PersonV1("Bob Charlie");
    }

    @GetMapping("/v2/person")
    public PersonV2 getSecondVersionOfPerson() {
        return new PersonV2(new Name("Samik", "Roy"));
    }

    @GetMapping(path = "/person", params = "version=1")
    public PersonV1 getPersonWithRequestParam() {
        return new PersonV1("Tanmoy Saha");
    }

    @GetMapping(path = "/person", params = "version=2")
    public PersonV2 getPersonWithRequestParam2() {
        return new PersonV2(new Name("Samik", "Roy"));
    }

    @GetMapping(path = "/person/header", headers = "X-API-VERSION=1")
    public PersonV1 getPersonWithRequestHeaderVersion1() {
        return new PersonV1("Tanmoy Saha");
    }

    @GetMapping(path = "/person/header", headers = "X-API-VERSION=2")
    public PersonV2 getPersonWithRequestHeaderVersion2() {
        return new PersonV2(new Name("Samik", "Roy"));
    }

    @GetMapping(path = "/person/accept", produces = "application/vnd.company.app-v1+json")
    public PersonV1 getPersonWithAcceptHeaderVersion1() {
        return new PersonV1("Tanmoy Saha");
    }

    @GetMapping(path = "/person/accept", produces = "application/vnd.company.app-v2+json")
    public PersonV2 getPersonWithAcceptHeaderVersion2() {
        return new PersonV2(new Name("Samik", "Roy"));
    }


}
