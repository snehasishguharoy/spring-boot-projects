package com.example.rest.webservices.restfulwebservices.filtering;

import com.example.rest.webservices.restfulwebservices.beans.Employee;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RestController
public class FilteringController {

    @GetMapping("/filetring")
    public MappingJacksonValue filtering() {
        Employee emp = new Employee("Samik", 23, 1200.0);
        MappingJacksonValue mappingJacksonValue = new MappingJacksonValue(emp);
        SimpleBeanPropertyFilter filter = SimpleBeanPropertyFilter.filterOutAllExcept("name", "age");
        FilterProvider filterProvider = new SimpleFilterProvider().addFilter("EmpFilter", filter);
        mappingJacksonValue.setFilters(filterProvider);
        return mappingJacksonValue;
    }

    @GetMapping("/filetring-list")
    public MappingJacksonValue filteringList() {
        List<Employee> list = Arrays.asList(new Employee("Samik", 22, 123.90),
                new Employee("Samrat", 32, 897.67),
                new Employee("Papa", 54, 761.23));
        MappingJacksonValue value = new MappingJacksonValue(list);
        SimpleBeanPropertyFilter filter = SimpleBeanPropertyFilter.filterOutAllExcept("name", "salary");
        FilterProvider filterProvider = new SimpleFilterProvider().addFilter("EmpFilter", filter);
        value.setFilters(filterProvider);
        return value;
    }
}
