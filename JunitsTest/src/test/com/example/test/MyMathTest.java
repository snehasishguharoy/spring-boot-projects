package test.com.example.test;

import com.example.test.MyMath;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MyMathTest {

    private   MyMath myMath=new MyMath();
    @Test
    void calculateSumTest(){
        assertEquals(6,myMath.calculateSum(new int[]{1,2,3}));
    }
    @Test
    void calculateSumZeroLengthArrayTest(){
        assertEquals(6,myMath.calculateSum(new int[]{}));
    }


}
