package test.com.example.test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

public class MyAssertTest {

    private List<String> todos= Arrays.asList("AWS","Azure","devops");

    @Test
    void test(){
        boolean test=todos.contains("AWS");
        Assertions.assertEquals(true,test);
        Assertions.assertEquals(3,todos.size());
        Assertions.assertTrue(test);
        Assertions.assertArrayEquals(new int[]{1,2,3},new int[]{1,2});
    }
}
