package com.example.oauth.oauthspringsecuritydemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OauthSpringSecurityDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(OauthSpringSecurityDemoApplication.class, args);

	}


}
