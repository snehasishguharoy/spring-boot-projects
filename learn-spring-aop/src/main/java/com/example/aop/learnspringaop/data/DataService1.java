package com.example.aop.learnspringaop.data;

import com.example.aop.learnspringaop.annotations.TrackTime;
import org.springframework.stereotype.Repository;

@Repository
public class DataService1 {

    @TrackTime
    public int[] getData() throws InterruptedException {
        Thread.sleep(1000);
        return new int[]{111, 222, 333, 444, 555};
    }
}
