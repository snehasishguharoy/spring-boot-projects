package com.example.aop.learnspringaop.aspects;

import org.aspectj.lang.annotation.Pointcut;

public class CommonPoinCutConfig {

    @Pointcut("execution(* com.example.aop.learnspringaop.*.*.*(..))")
    public void businessAndDataPackageConfig(){}

    @Pointcut("execution(* com.example.aop.learnspringaop.business.*.*(..))")
    public void businessPackageConfig(){}

    @Pointcut("execution(* com.example.aop.learnspringaop.data.*.*(..))")
    public void dataPackageConfig(){}

    @Pointcut("bean(*Service*)")
    public void dataPackageUsingBeans(){}

    @Pointcut("@annotation(com.example.aop.learnspringaop.annotations.TrackTime)")
    public void trackTimeAnnotation(){}
}
