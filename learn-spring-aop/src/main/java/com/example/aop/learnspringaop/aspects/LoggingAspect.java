package com.example.aop.learnspringaop.aspects;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;

@Configuration
@Aspect
public class LoggingAspect {

    Logger LOGGER = LoggerFactory.getLogger(getClass());

    // Poincut----when

    @Before("com.example.aop.learnspringaop.aspects.CommonPoinCutConfig.dataPackageUsingBeans()") // When to do
    public void logMethodCallBeforeExecution(JoinPoint point) {
        // Logic --- what
        LOGGER.info("Before Aspect -{} is called with arguments: {}", point.getArgs(), point.getArgs()); // What to do
    }

    @After("com.example.aop.learnspringaop.aspects.CommonPoinCutConfig.businessAndDataPackageConfig()") // When to do
    public void logMethodCallAfterExecution(JoinPoint point) {
        // Logic --- what
        LOGGER.info("After Aspect -{} has executed", point); // What to do
    }

    @AfterThrowing(pointcut = "com.example.aop.learnspringaop.aspects.CommonPoinCutConfig.businessAndDataPackageConfig()",
            throwing = "excp"
    ) // When to do
    public void logMethodCallAfterException(JoinPoint point, Exception excp) {
        // Logic --- what
        LOGGER.info("After Throwing Aspect -{} has thrown an exception {}", point, excp); // What to do
    }

    @AfterReturning(pointcut ="com.example.aop.learnspringaop.aspects.CommonPoinCutConfig.dataPackageConfig()",
            returning = "resValue")
    public void logMethodCallAfterSuccessfulExecution(JoinPoint point, Object resValue) {
        // Logic --- what
        LOGGER.info("After Returning Aspect -{} has returned {}", point, resValue); // What to do
    }


}
