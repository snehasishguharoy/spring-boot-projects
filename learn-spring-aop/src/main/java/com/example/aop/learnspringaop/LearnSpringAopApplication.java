package com.example.aop.learnspringaop;

import com.example.aop.learnspringaop.business.BusinessService1;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LearnSpringAopApplication implements CommandLineRunner {

    private Logger LOGGER = LoggerFactory.getLogger(LearnSpringAopApplication.class);

    private BusinessService1 service1;

    public LearnSpringAopApplication(BusinessService1 service1) {
        this.service1 = service1;
    }

    public static void main(String[] args) {
        SpringApplication.run(LearnSpringAopApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        LOGGER.info("value returned is {}",service1.calculateMax());

    }
}
