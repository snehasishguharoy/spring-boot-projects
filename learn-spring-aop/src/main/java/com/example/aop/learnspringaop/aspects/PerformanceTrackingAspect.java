package com.example.aop.learnspringaop.aspects;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;

@Configuration
@Aspect
public class PerformanceTrackingAspect {
    Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Around("com.example.aop.learnspringaop.aspects.CommonPoinCutConfig.trackTimeAnnotation()")
    public Object executionTime(ProceedingJoinPoint joinPoint) throws Throwable {
        // Start a timer
        long startTimeMillis = System.currentTimeMillis();
        // Execute the method
        Object proceed = joinPoint.proceed();
        // Stop the timer
        long stopTimeMillis = System.currentTimeMillis();
        long executionDuration = stopTimeMillis - startTimeMillis;
        LOGGER.info("Around Aspect -{} Method is executed in {}",joinPoint, executionDuration);
        return proceed;

    }
}
