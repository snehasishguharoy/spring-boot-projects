package com.example.jpa.springdatajpaexample.course;


import com.example.jpa.springdatajpaexample.course.spring.data.jpa.CourseSpringDataJPARepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class CourseCommandLineRunner implements CommandLineRunner {

//    @Autowired
//    private CourseJDBCRepo repo;

//    @Autowired
//    private CourseRepo repo;

    @Autowired
    private CourseSpringDataJPARepo repo;

    @Override
    public void run(String... args) throws Exception {
        repo.save(new Course(1, "Learn Azure JPA", "Samik"));
        repo.save(new Course(2, "Learn GCP JPA", "Samik"));
        repo.save(new Course(3, "Learn AWS JPA", "Samik"));
        repo.save(new Course(4, "Learn IBM Cloud JPA", "Samik"));
        repo.deleteById(2l);
        System.out.println(repo.findById(1l));
        System.out.println(repo.findById(3l));
        System.out.println(repo.findById(4l));

        System.out.println(repo.findAll());
        System.out.println(repo.count());
        System.out.println(repo.findByAuthor("Samik"));
        System.out.println(repo.findByName("Learn AWS JPA"));
        System.out.println(repo.findByName("Learn IBM Cloud JPA"));
    }
}
