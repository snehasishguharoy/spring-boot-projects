package com.example.jpa.springdatajpaexample.course.spring.data.jpa;

import com.example.jpa.springdatajpaexample.course.Course;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CourseSpringDataJPARepo extends JpaRepository<Course, Long> {

    List<Course> findByAuthor(String author);

    List<Course> findByName(String courseName);
}
