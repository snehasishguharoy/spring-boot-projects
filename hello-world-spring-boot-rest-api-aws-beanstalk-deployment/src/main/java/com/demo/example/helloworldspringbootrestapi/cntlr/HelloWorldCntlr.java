package com.demo.example.helloworldspringbootrestapi.cntlr;

import com.demo.example.helloworldspringbootrestapi.beans.Employee;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloWorldCntlr {

    @GetMapping("/hello-world")
    public String hello(){
        return "Hello World";
    }

    @GetMapping("/hello-world/{name}")
    public String helloWithPathParams(@PathVariable String name){
        return "Hello World "+name;
    }

    @GetMapping("/hello-world/bean")
    public ResponseEntity<Employee> helloBean(){
        Employee employee=new Employee("Samik",675.23,"A1001");
        return ResponseEntity.ok(employee);
    }
}
