package com.demo.example.helloworldspringbootrestapi.beans;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Employee {

    private String empName;
    private Double empSal;
    private String empId;

}
