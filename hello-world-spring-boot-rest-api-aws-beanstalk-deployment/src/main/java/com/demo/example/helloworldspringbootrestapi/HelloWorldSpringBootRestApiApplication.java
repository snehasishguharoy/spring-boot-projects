package com.demo.example.helloworldspringbootrestapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HelloWorldSpringBootRestApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(HelloWorldSpringBootRestApiApplication.class, args);
	}

}
