package com.example.mockito.mockitodemo.data;

import org.springframework.stereotype.Repository;

@Repository
public interface DataService {

    int[] retrieveAll();
}
