package com.example.mockito.mockitodemo.business;

import com.example.mockito.mockitodemo.data.DataService;
import org.springframework.stereotype.Service;

@Service
public class BusinessImpl {

    private DataService dataService;

    public BusinessImpl(DataService dataService) {
        this.dataService = dataService;
    }

    public int findGreatest() {
        int[] data = dataService.retrieveAll();
        int val = Integer.MIN_VALUE;
        for (int n : data) {
            if (n > val) {
                val = n;
            }
        }
        return val;
    }
}
