package com.example.mockito.mockitodemo.business;

import com.example.mockito.mockitodemo.data.DataService;

public class DataServiceStub2 implements DataService {
    @Override
    public int[] retrieveAll() {
        return new int[]{67};
    }
}
