package com.example.mockito.mockitodemo;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.List;

public class ListTest {

    @Test
    public void simpleTest() {
        List listMock = Mockito.mock(List.class);
        Mockito.when(listMock.size()).thenReturn(5);
        Assertions.assertEquals(5, listMock.size());
    }

    @Test
    public void multipleReturns() {
        List listMock = Mockito.mock(List.class);
        Mockito.when(listMock.size()).thenReturn(1).thenReturn(2);
        Assertions.assertEquals(1, listMock.size());
        Assertions.assertEquals(2, listMock.size());
        Assertions.assertEquals(2, listMock.size());
        Assertions.assertEquals(2, listMock.size());
        Assertions.assertEquals(2, listMock.size());

    }

    @Test
    public void specificParameters() {
        List listMock = Mockito.mock(List.class);
        Mockito.when(listMock.get(0)).thenReturn("Snehasish");
        Assertions.assertEquals("Snehasish", listMock.get(0));
        Assertions.assertEquals(null, listMock.get(1));


    }

    @Test
    public void genericParameters() {

        List listMock = Mockito.mock(List.class);
        Mockito.when(listMock.get(Mockito.anyInt())).thenReturn("Samik");
        Assertions.assertEquals("Samik", listMock.get(0));
        Assertions.assertEquals("Samik", listMock.get(1));


    }
}
