package com.example.mockito.mockitodemo.business;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class BusinessImplTest {



    @Test
    public void findGreatestTest(){
        DataServiceStub stub=new DataServiceStub();
        BusinessImpl business=new BusinessImpl(stub);
        int greatest = business.findGreatest();
        Assertions.assertEquals(45,greatest);

    }

    @Test
    public void findGreatestTest_withOneValue(){
       DataServiceStub2 stub2=new DataServiceStub2();
        BusinessImpl business=new BusinessImpl(stub2);
        int greatest = business.findGreatest();
        Assertions.assertEquals(67,greatest);

    }
}
