package com.example.mockito.mockitodemo.business;

import com.example.mockito.mockitodemo.data.DataService;

public class DataServiceStub implements DataService {
    @Override
    public int[] retrieveAll() {
        return new int[]{23, 34, 45};
    }
}
