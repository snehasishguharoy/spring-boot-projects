package com.example.mockito.mockitodemo.business;

import com.example.mockito.mockitodemo.data.DataService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class BusinessImplMockTest {

    @Mock
    private DataService dataService;

    @InjectMocks
    private BusinessImpl business;


    @Test
    public void findGreatestTest() {
        Mockito.when(dataService.retrieveAll()).thenReturn(new int[]{25,4,5});
        Assertions.assertEquals(25,  business.findGreatest());
    }

    @Test
    public void findGreatestTest_OneValue() {
        Mockito.when(dataService.retrieveAll()).thenReturn(new int[]{35});
        Assertions.assertEquals(35,  business.findGreatest());
    }

    @Test
    public void findGreatestTest_EmptyArray() {
        Mockito.when(dataService.retrieveAll()).thenReturn(new int[]{});
        Assertions.assertEquals(Integer.MIN_VALUE,  business.findGreatest());
    }

}
