package com.test.spring.boot.learnspringboot.cntlr;

import com.test.spring.boot.learnspringboot.cnfg.CurrencyServiceCnfg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1")
public class CurrencyCnfgCntlr {

    @Autowired
    private CurrencyServiceCnfg serviceCnfg;

    @GetMapping("/currency-cnfg")
    public CurrencyServiceCnfg retrieveAllCourses() {
        return serviceCnfg;
    }
}
