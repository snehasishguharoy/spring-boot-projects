package com.test.spring.boot.learnspringboot.cntlr;

import com.test.spring.boot.learnspringboot.dto.Course;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/v1")
public class CourseCntlr {


    @GetMapping("/courses")
    public List<Course> retrieveAllCourses() {
        return Arrays.asList(new Course(1, "Learn AWS", "Snehasish"),
                new Course(2, "Learn Devops", "Samik"),
                new Course(3,"Learn Azure","Rahul"),
                new Course(4,"Learn GCP","Romit"),
                new Course(5,"Java 8","Santu"),
                new Course(6,"Java 9","Santu")
        );
    }
}
