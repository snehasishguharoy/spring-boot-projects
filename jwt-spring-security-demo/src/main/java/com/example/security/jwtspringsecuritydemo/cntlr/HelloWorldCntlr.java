package com.example.security.jwtspringsecuritydemo.cntlr;

import jakarta.annotation.security.RolesAllowed;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloWorldCntlr {

    @GetMapping("/security")
    public String security() {
        return "Hello to the Security World";
    }

    @GetMapping("/hello")
   // @RolesAllowed({"USER1","ADMIN1"})
    @Secured({"ROLE_USER","ROLE_ADMIN"})
    public String hello() {
        return "Hello World";
    }

    @GetMapping("/users/{username}")
    @PreAuthorize("hasRole('USER') and #username==authentication.name")
    @PostAuthorize("returnObject.username=='Rahul'")
    public Employee users(@PathVariable String username) {
        return new Employee(username);
    }
}

record Employee(String username){};
