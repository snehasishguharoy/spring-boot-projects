package com.example.springdemo28minutes;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

@Component
@Lazy
public class B {

    A a;

    public B(A a) {
        System.out.println("Some Intialization logic");
        this.a = a;
    }

    public void doSomething(){
        System.out.println("Do Something");
    }
}
