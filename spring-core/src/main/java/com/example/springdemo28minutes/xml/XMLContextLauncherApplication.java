package com.example.springdemo28minutes.xml;


import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Arrays;


public class XMLContextLauncherApplication {

    public static void main(String[] args) {

        try (var ctx = new ClassPathXmlApplicationContext("contextconfiguration.xml")) {
            Arrays.stream(ctx.getBeanDefinitionNames()).forEach(t -> {
                System.out.println(t);
            });
            System.out.println(ctx.getBean("name"));
            System.out.println(ctx.getBean("age"));
        }
    }
}
