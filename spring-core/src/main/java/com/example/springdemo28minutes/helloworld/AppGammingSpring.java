package com.example.springdemo28minutes.helloworld;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Arrays;

public class AppGammingSpring {

    public static void main(String[] args) {
        // Launch a Spring Context
        // Configure the things that we want Spring to manage
        try (var ctx = new AnnotationConfigApplicationContext(HelloWorldCnfg.class)) {
            System.out.println(ctx.getBean("name"));
            System.out.println(ctx.getBean("age"));
            System.out.println(ctx.getBean("person"));
            System.out.println(ctx.getBean(Address.class));
            System.out.println(ctx.getBean("person2MethodCall"));
            System.out.println(ctx.getBean("person2Parameters"));
            Arrays.stream(ctx.getBeanDefinitionNames()).forEach(name -> {
                System.out.println(name);
            });
            System.out.println(ctx.getBean(Person.class));
            System.out.println(ctx.getBean("person4Qualifier"));
        }
    }
}
