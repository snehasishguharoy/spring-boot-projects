package com.example.springdemo28minutes.helloworld;

public record Person(String name, int age, Address address) {

}
