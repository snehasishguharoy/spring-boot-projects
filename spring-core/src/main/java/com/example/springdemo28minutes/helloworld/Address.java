package com.example.springdemo28minutes.helloworld;

public record Address(String addressLine1, String city) {
}
