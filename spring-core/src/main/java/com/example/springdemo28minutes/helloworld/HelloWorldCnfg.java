package com.example.springdemo28minutes.helloworld;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

;

@Configuration
public class HelloWorldCnfg {

    @Bean
    public String name() {
        return "Snehasish";
    }

    @Bean
    public int age() {
        return 32;
    }

    @Bean
    public Person person() {
        return new Person("Samik", 41,new Address("Main Street","New York"));
    }

    @Bean
    public Person person2Parameters(String name,int age,Address address) {
        return new Person(name,age,address);
    }

    @Bean
    @Primary
    public Person person3Parameters(String name,int age,Address address3) {
        return new Person(name,age,address3);
    }

    @Bean
    public Person person4Qualifier(String name,int age, @Qualifier("address3qualifier")Address address3) {
        return new Person(name,age,address3);
    }

    @Bean
    public Person person2MethodCall() {
        return new Person(name(),age(),address());
    }
    @Bean(name = "address2")
    @Primary
    public Address address() {
        return new Address("Baker Street", "London");
    }
    @Bean(name = "address3")
    @Qualifier("address3qualifier")
    public Address address3() {
        return new Address("Lake Road", "India");
    }
}
