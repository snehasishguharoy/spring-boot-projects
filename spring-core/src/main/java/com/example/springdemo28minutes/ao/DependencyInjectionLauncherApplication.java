package com.example.springdemo28minutes.ao;


import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;


@Configuration
@ComponentScan
public class DependencyInjectionLauncherApplication {

    public static void main(String[] args) {
        try (var ctx = new AnnotationConfigApplicationContext(DependencyInjectionLauncherApplication.class)) {
            System.out.println(  ctx.getBean(YourBusinessClass.class));


        }
    }
}
