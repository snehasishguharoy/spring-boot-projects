package com.example.springdemo28minutes;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan
public class BeanScopeLauncherApplication {

    public static void main(String[] args) {
        try (var ctx=new AnnotationConfigApplicationContext(BeanScopeLauncherApplication.class)){

            System.out.println(   ctx.getBean(NormalClass.class));
            System.out.println(   ctx.getBean(NormalClass.class));
            System.out.println(   ctx.getBean(NormalClass.class));

            System.out.println(   ctx.getBean(PrototypeClass.class));
            System.out.println(   ctx.getBean(PrototypeClass.class));
            System.out.println(   ctx.getBean(PrototypeClass.class));
            System.out.println(   ctx.getBean(PrototypeClass.class));

        }
    }
}
