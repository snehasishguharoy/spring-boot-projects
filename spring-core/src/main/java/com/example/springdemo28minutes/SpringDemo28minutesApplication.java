package com.example.springdemo28minutes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringDemo28minutesApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringDemo28minutesApplication.class, args);
	}

}
