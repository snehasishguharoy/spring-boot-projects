package com.example.springdemo28minutes.g1;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;

@Configuration
@ComponentScan
public class CDIContextLauncherApplication {

    public static void main(String[] args) {
        try (var ctx = new AnnotationConfigApplicationContext(CDIContextLauncherApplication.class)) {
            Arrays.stream(ctx.getBeanDefinitionNames()).forEach(it -> {
                System.out.println(it);
            });
            System.out.println(ctx.getBean(BusinessService.class).getDataService());
        }
    }
}
