package com.example.springdemo28minutes.business.service;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

@Repository
@Primary
public class MongoDbDataService implements DataService {
    @Override
    public int[] retrieveData() {
        return new int[]{11, 12, 13, 14, 15, 16, 17};
    }
}
