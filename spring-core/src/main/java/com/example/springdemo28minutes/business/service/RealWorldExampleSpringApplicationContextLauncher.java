package com.example.springdemo28minutes.business.service;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan
public class RealWorldExampleSpringApplicationContextLauncher {

    public static void main(String[] args) {
        try (var ctx = new AnnotationConfigApplicationContext(RealWorldExampleSpringApplicationContextLauncher.class)) {
            System.out.println(ctx.getBean(BusinessCalculationService.class).findMax());

        }

    }
}
