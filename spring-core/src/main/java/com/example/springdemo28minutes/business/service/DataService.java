package com.example.springdemo28minutes.business.service;

public interface DataService {

    int[] retrieveData();
}
