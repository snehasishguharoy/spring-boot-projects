package com.example.springdemo28minutes.business.service;


import org.springframework.stereotype.Repository;

@Repository
public class MySqlDataService implements DataService {
    @Override
    public int[] retrieveData() {
        return new int[]{1, 2, 3, 4, 5, 6};
    }
}
