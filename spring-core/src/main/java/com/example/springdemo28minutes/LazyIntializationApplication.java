package com.example.springdemo28minutes;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan
public class LazyIntializationApplication {

    public static void main(String[] args) {
        try (var ctx = new AnnotationConfigApplicationContext(LazyIntializationApplication.class)) {
            System.out.println("Intialization of context is completed");
            ctx.getBean(B.class).doSomething();
//            Arrays.stream(ctx.getBeanDefinitionNames()).forEach(t -> {
//                System.out.println(t);
//            });
        }
    }
}

