package com.example.springdemo28minutes.game;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class GameRunner {
    @Autowired
    @Qualifier("superContraGameQualifier")
    private GamingConsole superContraGameQualifier;
//    @Autowired
//    public GameRunner(@Qualifier("superContraGameQualifier") GamingConsole marioGame) {
//        this.marioGame = marioGame;
//    }

    public void run() {
        System.out.println("Running game: " + superContraGameQualifier);
        superContraGameQualifier.up();
        superContraGameQualifier.down();
        superContraGameQualifier.left();
        superContraGameQualifier.right();
    }
}
