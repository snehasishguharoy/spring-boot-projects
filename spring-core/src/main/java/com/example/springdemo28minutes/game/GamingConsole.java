package com.example.springdemo28minutes.game;

public interface GamingConsole {
    void up();

    void down();

    void left();

    void right();
}
