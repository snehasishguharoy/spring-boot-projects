insert into Todo (ID, USERNAME, DESCRIPTION, TARGET_DATE, DONE)
values(100,'Snehasish', 'Get AWS Certified', CURRENT_DATE(), false);

insert into Todo (ID, USERNAME, DESCRIPTION, TARGET_DATE, DONE)
values(101,'Snehasish', 'Get Azure Certified', CURRENT_DATE(), false);

insert into Todo (ID, USERNAME, DESCRIPTION, TARGET_DATE, DONE)
values(102,'Snehasish', 'Get GCP Certified', CURRENT_DATE(), false);