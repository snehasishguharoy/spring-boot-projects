package com.example.todo.app.myfirstwebapp.service;

import com.example.todo.app.myfirstwebapp.dto.Todo;
import jakarta.validation.Valid;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TodoService {

    private static List<Todo> todos = new ArrayList<>();
    private static int todosCount = 0;

    static {
        todos.add(new Todo(++todosCount, "Snehasish", "Learn AWS", LocalDate.now().plusYears(1), false));
        todos.add(new Todo(++todosCount, "Samik", "Learn Azure", LocalDate.now().plusYears(2), false));
        todos.add(new Todo(++todosCount, "Snehasish", "Learn Google Cloud Platform", LocalDate.now().plusYears(3), false));
    }

    public List<Todo> findByUserName(String username) {
        return todos.stream().filter(t -> t.getUsername().equalsIgnoreCase(username)).collect(Collectors.toList());
    }

    public void addTodo(String username, String desc, LocalDate targetDate, boolean isDone) {
        Todo todo = new Todo(++todosCount, username, desc, targetDate, isDone);
        todos.add(todo);
    }

    public void deleteById(int id) {

        todos.removeIf((todo -> {
            return todo.getId() == id;
        }));
    }


    public Todo findById(Integer id) {
        return todos.stream().filter(t -> t.getId() == id).findFirst().get();
    }

    public void updateTodo(@Valid Todo todo) {
        deleteById(todo.getId());
        todos.add(todo);

    }
}
