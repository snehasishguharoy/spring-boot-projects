package com.example.todo.app.myfirstwebapp.repo;

import com.example.todo.app.myfirstwebapp.dto.Todo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TodoRepo extends JpaRepository<Todo, Integer> {

    List<Todo> findByUsername(String username);
}
