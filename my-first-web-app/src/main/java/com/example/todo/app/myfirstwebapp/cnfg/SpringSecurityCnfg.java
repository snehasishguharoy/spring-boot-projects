package com.example.todo.app.myfirstwebapp.cnfg;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
public class SpringSecurityCnfg {


    @Bean
    public InMemoryUserDetailsManager createUserDetails() {

        UserDetails userDetails1 = createNewUser("Snehasish", "Java@786");
        UserDetails userDetails2 = createNewUser("Samik", "dummy");
        return new InMemoryUserDetailsManager(userDetails1, userDetails2);

    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }


    private UserDetails createNewUser(String username, String password) {
        return User
                .builder()
                .passwordEncoder((t) -> passwordEncoder().encode(t))
                .username(username)
                .password(password)
                .roles("USER", "ADMIN")
                .build();
    }

    // ALL Urls's are protected
    // A login form is shown for unauthorized requests
    // CSRF disable
    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http.authorizeHttpRequests(auth -> auth.anyRequest().authenticated());
        http.formLogin(Customizer.withDefaults());
        http.csrf().disable();
        http.headers().frameOptions().disable();
        return http.build();
    }


}
